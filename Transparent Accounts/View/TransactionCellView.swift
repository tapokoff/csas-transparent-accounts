//
//  TransactionCellView.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

struct TransactionCellView: View {
    @State var value: Double
    @State var currency: String
    @State var typeDescription: String
    @State var dueDate: String
    
    var body: some View {
        HStack {
            VStack {
                Text(typeDescription)
                    .frame(maxWidth: .infinity, alignment: .leading)
                Text("\(String(dueDate[dueDate.startIndex..<dueDate.index(dueDate.startIndex, offsetBy: 10)])) - \(String(dueDate[dueDate.index(dueDate.startIndex, offsetBy: 11)..<dueDate.endIndex]))")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 1)
            }
            Spacer()
            VStack {
                if value > 0 {
                    Text("\(String(value)) \(currency)")
                        .foregroundColor(.green)
                } else {
                    Text("\(String(value)) \(currency)")
                }
            }
        }
        .frame(maxWidth: .infinity)
    }
}

struct TransactionCellView_Previews: PreviewProvider {
    static var previews: some View {
        TransactionCellView(value: 777.00, currency: "CZK", typeDescription: "Daň z úroku", dueDate: "2017-05-31T00:00:00")
            .previewLayout(.sizeThatFits)
    }
}
