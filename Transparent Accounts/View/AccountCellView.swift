//
//  AccountRowView.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

struct AccountCellView: View {
    // MARK: PROPERTIES
    @State var account: Account
    
    // MARK: BODY
    var body: some View {
        HStack {
            Image(systemName: "person.crop.circle")
                .resizable()
                .frame(width: 60.0, height: 60.0)
            VStack(alignment: .leading) {
                Text(account.accountNumber)
                    .foregroundColor(.primary)
                    .font(.title3)
                Text(account.iban)
                    .font(.system(size: 10))
                    .foregroundColor(.secondary)
                Text(account.name)
                    .font(.footnote)
                    .foregroundColor(.secondary)
            }
            .padding(.leading, 10)
            Spacer()
        }
        .frame(maxWidth: .infinity, maxHeight: 100)
    }
}

// MARK: PREVIEW
struct AccountRowView_Previews: PreviewProvider {
    static var previews: some View {
        AccountCellView(account: Account(accountNumber: "000000-0109213309", bankCode: "0800", transparencyFrom: "2015-01-24T00:00:00", transparencyTo: "3000-01-01T00:00:00", publicationTo: "3000-01-01T00:00:00", actualizationDate: "2018-01-17T13:00:00", balance: 165939.97, currency: "CZK", name: "Společenství Praha 4, Obětí 6.května 553", iban: "CZ75 0800 0000 0001 0921 3309"))
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
