//
//  AccountTextRowView.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

struct AccountTextRowView: View {
    // MARK: PROPERTIES
    @State var first: String
    @State var second: String
    
    
    // MARK: BODY
    var body: some View {
        HStack {
            Text(first)
            Spacer()
            Text(second)
                .foregroundColor(.secondary)
        }
        .padding()
    }
}

// MARK: PREVIEW
struct AccountTextRowView_Previews: PreviewProvider {
    static var previews: some View {
        AccountTextRowView(first: "Account number:", second: "000000-0109213309")
            .previewLayout(.sizeThatFits)
    }
}
