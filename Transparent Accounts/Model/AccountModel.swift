//
//  AccountModel.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import Foundation

struct Account: Decodable {
    var accountNumber: String
    var bankCode: String
    var transparencyFrom: String
    var transparencyTo: String
    var publicationTo: String
    var actualizationDate: String
    var balance: Double
    var currency: String?
    var name: String
    var description: String?
    var note: String?
    var iban: String
}


/*
 
 {
     "accountNumber": "000000-0109213309",
     "bankCode": "0800",
     "transparencyFrom": "2015-01-24T00:00:00",
     "transparencyTo": "3000-01-01T00:00:00",
     "publicationTo": "3000-01-01T00:00:00",
     "actualizationDate": "2018-01-17T13:00:00",
     "balance": 165939.97,
     "currency": "CZK",
     "name": "Společenství Praha 4, Obětí 6.května 553",
     "iban": "CZ75 0800 0000 0001 0921 3309"
 }
 
 */
