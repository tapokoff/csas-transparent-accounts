//
//  TransactionModel.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import Foundation

struct Transaction: Identifiable, Decodable {
    var id: UUID?
    var amount: Amount
    var type: String
    var dueDate: String
    var processingDate: String
    var sender: Sender
    var receiver: Receiver
    var typeDescription: String
}

struct Amount: Decodable {
    var value: Double
    var precision: Double
    var currency: String
}

struct Sender: Decodable {
    var accountNumber: String
    var bankCode: String
    var iban: String
    var specificSymbol: String?
    var specificSymbolParty: String?
    var variableSymbol: String?
    var constantSymbol: String?
    var name: String?
    var description: String?
}

struct Receiver: Decodable {
    var accountNumber: String
    var bankCode: String
    var iban: String
}


/*
 
 
 {
     "amount": {
         "value": -1.21,
         "precision": 0,
         "currency": "CZK"
     },
     "type": "40900",
     "dueDate": "2017-12-31T00:00:00",
     "processingDate": "2018-01-01T00:00:00",
     "sender": {
         "accountNumber": "000000-0000000000",
         "bankCode": "0800",
         "iban": "CZ13 0800 0000 0029 0647 8309",
         "specificSymbol": "0000000000",
         "specificSymbolParty": "0000000000",
         "constantSymbol": "0000"
     },
     "receiver": {
         "accountNumber": "000000-2906478309",
         "bankCode": "0800",
         "iban": "CZ13 0800 0000 0029 0647 8309"
     },
     "typeDescription": "Daň z úroku"
 }
 
 
 */
