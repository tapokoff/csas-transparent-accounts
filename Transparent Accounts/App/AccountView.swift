//
//  AccountView.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

struct AccountView: View {
    @EnvironmentObject var network: Network
    @State private var pickedSegment = 0
    @State var account: Account
    @State var searchText: String = ""
    
    var body: some View {
        VStack {
            VStack {
                Text(account.accountNumber)
                    .foregroundColor(.primary)
                    .font(.title2)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading)
                Text("Balance: \(String(account.balance)) \(account.currency ?? "")")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.leading)
            }
            Picker("", selection: $pickedSegment) {
                Text("Transactions").tag(0)
                Text("Info").tag(1)
            }
            .pickerStyle(.segmented)
            .padding([.top,.leading,.trailing])
            if pickedSegment == 0 {
                TextField("Search", text: $searchText)
                    .padding()
                    .frame(height: 45)
                    .background(.gray.opacity(0.1))
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .padding([.trailing, .leading])
                    .overlay {
                        HStack {
                            Spacer()
                            Button {
                                searchText = ""
                            } label: {
                                Label("clear", systemImage: "xmark.circle.fill")
                                    .foregroundColor(.gray)
                                    .opacity(searchText.isEmpty ? 0 : 1)
                                    .padding(30)
                            }
                            .labelStyle(.iconOnly)
                        }
                    }
                    .onSubmit {
                        network.fetchTransactions(accountNumber: account.accountNumber, filter: searchText == "" ? nil : searchText)
                    }
                    .autocorrectionDisabled(true)
                List {
                    ForEach(network.transactions) { transaction in
                        TransactionCellView(value: transaction.amount.value, currency: transaction.amount.currency, typeDescription: transaction.typeDescription, dueDate: transaction.dueDate)
                    }
                }
                .refreshable {
                    network.fetchTransactions(accountNumber: account.accountNumber, filter: searchText == "" ? nil : searchText)
                }
                
            } else {
                ScrollView {
                    AccountTextRowView(first: "Bank code:", second: account.bankCode)
                    AccountTextRowView(first: "Transparency from:", second: account.transparencyFrom)
                    AccountTextRowView(first: "Transparency to:", second: account.transparencyTo)
                    AccountTextRowView(first: "Publication to:", second: account.publicationTo)
                    AccountTextRowView(first: "Actualization:", second: account.actualizationDate)
                    AccountTextRowView(first: "Name:", second: account.name)
                    AccountTextRowView(first: "IBAN:", second: account.iban)
                }
            }
        }
        .navigationTitle("Account info")
        .onAppear {
            network.fetchTransactions(accountNumber: account.accountNumber, filter: searchText == "" ? nil : searchText)
        }
        .onDisappear {
            network.clearTransactions()
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AccountView(account: Account(accountNumber: "000000-0109213309", bankCode: "0800", transparencyFrom: "2015-01-24T00:00:00", transparencyTo: "3000-01-01T00:00:00", publicationTo: "3000-01-01T00:00:00", actualizationDate: "2018-01-17T13:00:00", balance: 165939.97, currency: "CZK", name: "Společenství Praha 4, Obětí 6.května 553", iban: "CZ75 0800 0000 0001 0921 3309"))
                .environmentObject(Network())
        }
        .navigationViewStyle(.stack)
    }
}
