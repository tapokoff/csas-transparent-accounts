//
//  ContentView.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

struct ContentView: View {
    // MARK: PROPERTIES
    @EnvironmentObject var network: Network
    @State var searchText: String = ""
    
    // MARK: BODY
    var body: some View {
        NavigationView {
            VStack {
                TextField("Search", text: $searchText)
                    .padding()
                    .frame(height: 45)
                    .background(.gray.opacity(0.1))
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .padding(/*[.top, .leading, .trailing]*/)
                    .overlay {
                        HStack {
                            Spacer()
                            Button {
                                searchText = ""
                            } label: {
                                Label("clear", systemImage: "xmark.circle.fill")
                                    .foregroundColor(.gray)
                                    .opacity(searchText.isEmpty ? 0 : 1)
                                    .padding(30)
                            }
                            .labelStyle(.iconOnly)
                        }
                    }
                    .onSubmit {
                        network.fetchAccounts(filter: searchText == "" ? nil : searchText)
                    }
                    .autocorrectionDisabled(true)
                List {
                    ForEach(network.accounts, id: \.accountNumber) { account in
                        NavigationLink(destination: AccountView(account: account)) {
                            AccountCellView(account: account)
                                .padding(.vertical, 4)
                        }
                    }
                }
            }
            .navigationTitle("Accounts")
            .refreshable {
                network.fetchAccounts(filter: searchText == "" ? nil : searchText)
            }
        }
        .onAppear {
            network.fetchAccounts(filter: searchText == "" ? nil : searchText)
        }
        .navigationViewStyle(.stack)
    }
}

// MARK: PREVIEW
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(Network())
    }
}
