//
//  Transparent_AccountsApp.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

@main
struct Transparent_AccountsApp: App {
    var network = Network()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(network)
        }
    }
}
