//
//  API.swift
//  Transparent Accounts
//
//  Created by Dan on 15.11.2022.
//

import SwiftUI

extension String{
    var encodeUrl : String
    {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    var decodeUrl : String
    {
        return self.removingPercentEncoding!
    }
}

enum API {
    enum Types {
        enum Response {
            struct AccountList: Decodable {
                var accounts: [Account]
            }
            struct TransactionList: Decodable {
                var transactions: [Transaction]
            }
        }
        
        enum Endpoint: String {
            case accountList = "/"
            case transations = "transactions/"
        }
    }
}

class Network: ObservableObject {
    private let baseUrl = "https://webapi.developers.erstegroup.com/api/csas/public/sandbox/v3/transparentAccounts"
    private let apiKey = "871afc23-018a-4abe-af8e-b5f757e5c45c"
    @Published var accounts: [Account] = []
    @Published var transactions: [Transaction] = []
    
    func fetchAccounts(filter: String? = nil) {
        let urlString = baseUrl + API.Types.Endpoint.accountList.rawValue + (filter != nil ? "?filter=\(filter!)" : "")
        guard let url = URL(string: urlString.encodeUrl) else {fatalError("URL is missing")}
        
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue(apiKey, forHTTPHeaderField: "WEB-API-key")

        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }

            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let decodedAccounts = try JSONDecoder().decode(API.Types.Response.AccountList.self, from: data)
                        self.accounts = decodedAccounts.accounts
                    } catch let error {
                        print("Error decoding: ", error)
                    }
                }
            }
        }

        dataTask.resume()
    }
    
    func fetchTransactions(accountNumber: String, filter: String? = nil) {
        let urlString = baseUrl + "/" + accountNumber + "/" + API.Types.Endpoint.transations.rawValue + (filter != nil ? "?filter=\(filter!)" : "")
        guard let url = URL(string: urlString.encodeUrl) else {fatalError("URL is missing")}
        
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue(apiKey, forHTTPHeaderField: "WEB-API-key")

        let dataTask = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print("Request error: ", error)
                return
            }

            guard let response = response as? HTTPURLResponse else { return }

            if response.statusCode == 200 {
                guard let data = data else { return }
                DispatchQueue.main.async {
                    do {
                        let decodedAccounts = try JSONDecoder().decode(API.Types.Response.TransactionList.self, from: data)
                        self.transactions = decodedAccounts.transactions
                        for i in 0..<self.transactions.count {
                            self.transactions[i].id = UUID()
                        }
                    } catch let error {
                        print("Error decoding: ", error)
                    }
                }
            }
        }

        dataTask.resume()
    }
    
    func clearTransactions(){
        transactions = []
    }
}
