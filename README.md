# CSAS Transparent Accounts

## Why?

This application can search for transparent accounts in CSAS network, and give some info about they. 

## Technology

I used the following technologies:

- Swift
- SwiftUI 
- REST
- URLSessions and tasks for requests

## Interface

Just simple and intuitive interface ( not designer, sorry ;) )